###' Make fake data for plim model
###'
###' Generate some sample points and times
###'
###' @param seed random seed
###' @param nregions number of regions
###' @param ncases number of cases
###' @param ncovars number of covariates
###' @param window timespan of window
###'
###' @return a list of cases, regions, populations, covariate matrix generator,
###'  adjacency matric generator
###' @export
###' @examples
###' makeData()
makeData <- function(seed=1, nregions=12, ncases=10, ncovars=2, window=0.3){
  set.seed(seed)

  if(nregions<10){
    stop(paste(nregions," is not enough regions",sep=""))
  }
  if(ncases<10){
    stop(paste(ncases," is not enough cases",sep=""))
  }

  if(ncovars<0){
    stop("must have zero or more covariates")
  }
  
  nchars=ceiling(log(nregions,base=26))
  
  R = LETTERS
  if(nregions>26){
    for(i in 1:(nchars-1)){
      R = outer(R,letters,paste,sep="")
    }
  }
  R = R[1:nregions]

  if(ncovars>0){
    covars = matrix(rnorm(ncovars*nregions),ncol=ncovars)
    Z = constantCovariates(covars,R=R)
  }else{
    Z = constantCovariates()
  }

  pop = as.integer(500*(10+runif(nregions)))
  cases = data.frame(
    time=sort(runif(ncases)),
    region=sample(R,ncases,replace=TRUE)
    )
  cases = stCases(cases,"region","time")

  Wm = matrix(runif(nregions*nregions)>.7,nregions)
  Wm = Wm + t(Wm)
  W = constantTrans(1 * (Wm > 0)+0.1,R=R)
  
  return(list(cases=cases,R=R,pop=pop,Z=Z,W=W,window=window))
}
  

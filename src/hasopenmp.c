#ifdef _OPENMP

#include <omp.h>
void hasopenmp(int *hasit){
  *hasit=omp_get_max_threads();
  return;
}

#else

void hasopenmp(int *hasit){
  *hasit = 0;
  return;
}

#endif

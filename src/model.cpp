
#include <RcppArmadillo.h>

RcppExport SEXP betax(SEXP beta, SEXP X) {
BEGIN_RCPP
  Rcpp::NumericMatrix Xm(X);
  Rcpp::NumericVector betav(beta);

  int nCovar = Xm.nrow();
  int nData = Xm.ncol();
  
  arma::mat Xa(Xm.begin(), nCovar, nData,false);
  arma::colvec B(betav.begin(), betav.size(), false);

  arma::colvec XaB = exp(Xa * B);

  arma::mat Xa2 = Xa % Xa;

  return Rcpp::List::create(
			    Rcpp::Named("results")=XaB,
			    Rcpp::Named("squared")=Xa2
			    );
END_RCPP
}

RcppExport SEXP lambda(SEXP alpha, SEXP beta, SEXP Z, SEXP W){
BEGIN_RCPP
  Rcpp::NumericVector alphaV(alpha);
  Rcpp::NumericVector betaV(beta);
  int nCovar = betaV.size();
  
  Rcpp::NumericMatrix Zm(Z); // nRegions * nCovar
  Rcpp::NumericMatrix Wm(W);
  int nRegions = Wm.nrow(); // must equal Wm.ncol()

  arma::mat mZm(Zm.begin(), nRegions, nCovar, false);
  arma::mat mWm(Wm.begin(), nRegions, nRegions, false);
  arma::colvec vAlphav(alphaV.begin(), alphaV.size(),false);
  arma::colvec vBetav(betaV.begin(), betaV.size(),false);


  arma::mat lambda = (exp(mZm * vAlphav) * trans(exp(mZm*vBetav)) ) % mWm;

  return Rcpp::List::create(
			    Rcpp::Named("lambda") = lambda
			    );

END_RCPP
}

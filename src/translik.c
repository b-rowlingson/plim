#include "math.h"
#include <stdio.h>
void dumpMatrixI(int a[], int n1, int n2);
void dumpMatrix(double a[], int n1, int n2);

int andex(int i, int j, int n1){
  return(j*n1+i);
}

void translik(double alpha[], double beta[], int *ncovars,
	      double ct[], int cr[], int *ncases,
	      double pop[], int *nregions,
	      double Z[], /* nregions * ncovars, or anything if *ncovars==0 */
	      double W[],  /* nregions * nregions */
	      int mC[], /* ncases * nregions */
	      double lambda[], /* nregions * nregions workspace*/
	      double *ll
	      ){
  int iregion, jregion, icovar, icase;
  int caseRegion;
  double A,B;
  double top, b1, b2;
  int topCount;
  double lll;
  double mcj;

  for(iregion = 0 ;iregion< *nregions; iregion++){
    A = 0;
    // loop skipped if *ncovars=0
    for(icovar = 0; icovar < *ncovars; icovar++){ 
      A = A + Z[andex(iregion,icovar,*nregions)] * alpha[icovar];
    }

    A = exp(A);

    for(jregion = 0 ;jregion< *nregions; jregion++){
      B = 0;
      // loop skipped if *ncovars=0
      for(icovar = 0; icovar < *ncovars; icovar++){ 
	B = B + Z[andex(jregion,icovar,*nregions)] * beta[icovar];
      }
      B=exp(B);
      lambda[andex(iregion,jregion,*nregions)] = A * B * W[andex(iregion,jregion,*nregions)];
    }
  }

  lll = 0;

#pragma omp parallel for private(icase, b1, iregion,jregion,b2,top,topCount,caseRegion,mcj) reduction(+:lll) schedule(static,1)

  for(icase = 0; icase < *ncases; icase++){

    caseRegion = cr[icase]-1; /* C indexes from zero, so subtract one from 1-based index */
    top = 0;
    topCount = 0;
    for(iregion = 0; iregion < *nregions; iregion++){
      topCount += mC[andex(icase,iregion,*ncases)];
      top = top + mC[andex(icase,iregion,*ncases)] * lambda[andex(iregion,caseRegion,*nregions)];
    }

    if(topCount > 0){

      top = top * pop[caseRegion];
      b1 = 0;

    //#pragma omp parallel for private(iregion,jregion,b2) reduction(+:b1) schedule(static,1)

      for(iregion = 0; iregion < *nregions; iregion++){
	b2 = 0;
	for(jregion = 0; jregion < *nregions; jregion++){
	  mcj = (float)mC[andex(icase,jregion,*ncases)]; 
	  b2 = b2 + mcj * lambda[andex(jregion,iregion,*nregions)];
	}
	b1 = b1 + pop[iregion] * b2;
      }
      lll = lll + log(top/b1);
    }
  }
  
  *ll = lll;
}

void dumpVector(double v[],int n){
  int i;
  for(i=0;i<n;i++){
    printf("[%d] = %f | ",i,v[i]);
  }
  printf("\n");
}

void dumpMatrix(double a[], int n1, int n2){
  int i,j;
  int b1,b2;
  b1 = (n1 > 10 ? 10 : n1);
  b2 = (n2 > 10 ? 10 : n2);

  printf("dumping... %d by %d\n",n1,n2);
  for(i=0;i<b1;i++){
    printf("%2d: ",i);
    for(j=0;j<b2;j++){
      printf("%2f ",a[andex(i,j,n1)]);
    }
    printf(" ... ...\n");
  }
  printf("done\n");
}
void dumpMatrixI(int a[], int n1, int n2){
  int i,j;  
  int b1,b2;

  b1 = (n1 > 10 ? 10 : n1);
  b2 = (n2 > 10 ? 10 : n2);


  printf("dumping integers...\n");
  for(i=0;i<b1;i++){
    printf("%2d: ",i);
    for(j=0;j<b2;j++){
      printf("%2d ",a[andex(i,j,n1)]);
    }
    printf(" ... ... \n");
  }
  printf("done\n");
}

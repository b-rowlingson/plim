library(plim)

m = makeData(seed=123)
mc = laply(caseTimes(m$cases,numeric=TRUE), function(x) {mCount(x, m$window, m$cases, m$R) })
xy = matrix(runif(length(m$R)*2),ncol=2)
Wd =distDecayTrans(xy,R=m$R)

testset=function(p,cases,pop,Z,W,mc){
  Rp = transModelLik(p,cases,pop=pop,Z=Z,W=W,mC=mc)
  Cp = transModelCLik(p,cases,pop=pop,Z=Z,W=W,mC=mc)
  test = all.equal(Rp,Cp)
  if(!isTRUE(test)){
    cat("FAIL p = ",p,"\n")
    cat(test,"\n")
    cat("logL with R = ",Rp,"\n")
    cat("logL with C = ",Cp,"\n\n")
  }else{
    cat("PASS p =(",p,"), Logl = ",Rp,"\n")
  }
}

ptests = list(
  c(alpha=0,alpha=0,beta=0,beta=0),
  c(alpha=0.6,alpha=0,beta=0,beta=0),
  c(alpha=0,alpha=0.5,beta=0,beta=0),
  c(alpha=0.4,alpha=0.5,beta=0,beta=0),
  c(alpha=0,alpha=0,beta=-0.2,beta=0),
  c(alpha=0,alpha=0,beta=-0.2,beta=0.3),
  c(alpha=-0.2,alpha=0,beta=-0.2,beta=0.3),
  c(alpha=0.2,alpha=0.5,beta=-0.2,beta=0.3)
 )

gtests = list(
  c(gamma=0,gamma=0),
  c(gamma=-pi,gamma=0),
  c(gamma=pi,gamma=0),
  c(gamma=0,gamma=pi),
  c(gamma=0,gamma=-pi),
  c(gamma=-2,gamma=-2.3)
  )

### just alpha/beta models
for(p in ptests){
  testset(p,m$cases,m$pop,m$Z,m$W,mc)
}

### just gamma models
Z0=constantCovariates(R=m$R)
for(g in gtests){
  testset(g,m$cases,m$pop,Z0,Wd,mc)
}

for(p in ptests){
  for(g in gtests){
    pg = c(p,g)
    testset(pg,m$cases,m$pop,m$Z,Wd,mc)
  }
}

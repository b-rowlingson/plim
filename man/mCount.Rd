\name{mCount}
\alias{mCount}
\title{Number of people infectious in regions at time t...}
\usage{mCount(t, window, cases, R)}
\description{Number of people infectious in regions at time t}
\details{Return the number of infectious individuals in regions at given times.}
\value{number of infections at time t}
\arguments{\item{t}{the time}
\item{window}{length of time window}
\item{cases}{case data}
\item{R}{region codes}}

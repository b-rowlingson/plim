\name{plimlik}
\alias{plimlik}
\title{compute a likelihood for the plim model...}
\usage{plimlik(paramList, cases, window, pop, Z, W, mc, code="C")}
\description{compute a likelihood for the plim model}
\details{just returns one likelihood value for the parameters}
\value{the log-likelihood}
\arguments{\item{paramList}{list of alpha, beta, gamma values}
\item{cases}{region-time case data}
\item{window}{infectivity timespan}
\item{pop}{population vector}
\item{Z}{covariate generator}
\item{W}{transmissivity generator}
\item{mc}{count of infectious cases per region per day - computed if not given}
\item{code}{whether to use "C" or "R" code.}}

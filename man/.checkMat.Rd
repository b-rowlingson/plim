\name{.checkMat}
\alias{.checkMat}
\title{basic sanity checks...}
\usage{.checkMat(mat, R)}
\description{basic sanity checks}
\details{2d square matrix with correct region codes}
\arguments{\item{mat}{square matrix}
\item{R}{region codes}}

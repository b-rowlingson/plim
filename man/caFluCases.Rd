\name{caFluCases}
\docType{data}
\alias{caFluCases}
\title{California Flu Cases, approximately}
\description{
  This is a dataset of swine flu cases scraped from various
  notifications of swine flu. MASSIVE WARNING: DO NOT TAKE
  THIS AS ANYTHING LIKE DEFINITIVE. Honestly, the data was so
  patchy I had to fill it in like a crumbling plaster wall. But
  its a useful semi-real dataset for an example. If I can get
  official swine flu cases by day and county then I'll use them.
}
\usage{caFluCases}

\format{
  A data frame with 2464 observations on the following 2 variables.
  \describe{
    \item{\code{region}}{a FIPC county code}
    \item{\code{day}}{the alleged day of possible flu onset plus or
  minus something random}
  }
}

\source{Scraped from weekly PDF reports, randomly assigned to days
  within the week, and gaps filled in with runif()}

\keyword{datasets}

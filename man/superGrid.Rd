\name{superGrid}
\alias{superGrid}
\title{return slices, lines, cubes from a full grid...}
\usage{superGrid(..., dimension)}
\description{return slices, lines, cubes from a full grid}
\value{matrix of values in n-dimensional parts}
\arguments{\item{...}{vectors spanning the space (all odd length)}
\item{dimension}{dimension of the returned features}}
